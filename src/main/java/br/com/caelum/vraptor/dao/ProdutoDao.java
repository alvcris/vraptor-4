package br.com.caelum.vraptor.dao;

import br.com.caelum.vraptor.model.Produto;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@RequestScoped
public class ProdutoDao {

    @Inject
    private EntityManager em;

    public ProdutoDao() {
    }

    public void adiciona(Produto produto) {
        em.persist(produto);
    }

    public void remove(Produto produto) {
        em.remove(busca(produto));
    }

    public Produto busca(Produto produto) {
        return em.find(Produto.class, produto.getId());
    }

    @SuppressWarnings("unchecked")
    public List<Produto> lista() {
        return em.createQuery("select p from Produto p").getResultList();
    }
}