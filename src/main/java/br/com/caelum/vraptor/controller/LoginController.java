package br.com.caelum.vraptor.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.annotation.Public;
import br.com.caelum.vraptor.dao.UsuarioDao;
import br.com.caelum.vraptor.model.Usuario;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.validator.Validator;

import javax.inject.Inject;

@Controller
public class LoginController {

    @Inject
    private UsuarioDao usuarioDao;

    @Inject
    private Validator validator;

    @Inject
    private Result result;

    @Inject
    private LoggedUser loggedUser;

    @Get
    @Public
    public void login() {

    }

    @Post
    @Public
    public void auth(final Usuario usuario) {
        if (!usuarioDao.existe(usuario)) {
            validator.add(new I18nMessage("null", "null"));
            validator.onErrorUsePageOf(this).login();
        }
        loggedUser.setUsuario(usuario);
        result.include("user", usuario);
        result.redirectTo(ProdutoController.class).lista();
    }

}
