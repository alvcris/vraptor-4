package br.com.caelum.vraptor.controller;

import br.com.caelum.vraptor.model.Usuario;
import lombok.Getter;
import lombok.Setter;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Getter
@Setter
@SessionScoped
@Named
public class LoggedUser implements Serializable {

    private Usuario usuario;
}
