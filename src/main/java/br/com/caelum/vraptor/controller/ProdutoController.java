package br.com.caelum.vraptor.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.dao.ProdutoDao;
import br.com.caelum.vraptor.model.Produto;
import br.com.caelum.vraptor.validator.Validator;
import br.com.caelum.vraptor.view.Results;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;

@Controller
public class ProdutoController {
    @Inject
    private Result result;

    @Inject
    private ProdutoDao produtoDao;

    @Inject
    private Validator validator;

    @Get("/")
    public void inicio() {

    }

    @Get
    public List<Produto> lista() {
        return produtoDao.lista();
    }

    @Get
    public void listaJson() {
        result.use(Results.json()).from(produtoDao.lista()).serialize();
    }

    @Get
    public void form() {

    }

    @Post
    public void add(@Valid final Produto produto) {
        validator.onErrorRedirectTo(this).form();
        produtoDao.adiciona(produto);
        result.redirectTo(this).lista();
    }

}
