package br.com.caelum.vraptor.interceptor;

import br.com.caelum.vraptor.Accepts;
import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.annotation.Public;
import br.com.caelum.vraptor.controller.ControllerMethod;
import br.com.caelum.vraptor.controller.LoggedUser;
import br.com.caelum.vraptor.controller.LoginController;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.inject.Inject;

@Intercepts
@NoArgsConstructor
public class AuthInterceptor {

    @Inject
    private LoggedUser loggedUser;

    @Inject
    private Result result;

    @Inject
    private ControllerMethod controllerMethod;

    @Accepts
    public boolean accepts(){
        return !controllerMethod.containsAnnotation(Public.class);
    }

    @AroundCall
    public void intercepts(SimpleInterceptorStack simpleInterceptorStack){
        if(loggedUser.getUsuario() == null){
            result.redirectTo(LoginController.class).login();
            return;
        }

        simpleInterceptorStack.next();
    }
}
