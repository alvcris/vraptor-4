<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />
       <link rel="stylesheet" type="text/css" href="../base.css" />
       <title>Login</title>
    </head>

    <body>
        <div class="container">
            <form action="<c:url value='/login/auth' />" method="post">
                Nome: <input class="form-control" type="text" name="usuario.nome"/>
                Valor: <input class="form-control" type="text" name="usuario.senha"/>
                <input class="btn btn-primary" type="submit" value="Login"/>
            </form>
        </div>
    </body
</html>