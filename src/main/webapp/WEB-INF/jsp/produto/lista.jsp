<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h1>Lista de Produtos do ${loggedUser}</h1>
<table>
    <c:forEach var="produto" items="${produtoList}">
    <tr>
        <td>${produto.nome}</td>
        <td>${produto.valor}</td>
        <td>${produto.quantidade}</td>
    </tr>
    </c:forEach>
</table>