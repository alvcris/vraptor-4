<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />
       <link rel="stylesheet" type="text/css" href="../base.css" />
       <title>Novo Produto</title>
    </head>

    <body>
        <div class="container">
            <h1>Adiciona Produto </h1>

            <form action="<c:url value='/produto/add' />" method="post">
                Nome: <input class="form-control" type="text" name="produto.nome"/>
                Valor: <input class="form-control" type="text" name="produto.valor"/>
                Quantidade: <input class="form-control" type="text" name="produto.quantidade"/>
                <input class="btn btn-primary" type="submit" value="Adiciona"/>
            </form>
        </div>
    </body
</html>